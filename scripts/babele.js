// Credits to https://github.com/DjLeChuck
// https://github.com/DjLeChuck/foundryvtt-pf1-fr-babele/blob/main/scripts/register.js
import converters, { tContainerItems } from './converters.js';
import { OriginalNameFilter } from './filters.mjs';

Hooks.on('init', () => {
  game.settings.register('pf1e-de', 'autoRegisterBabel', {
    name: 'Automatische Aktivierung der Übersetzung über Babele',
    hint: 'Setzt Übersetzungen innerhalb von Babele automatisch, ohne auf das Verzeichnis mit den Übersetzungen verweisen zu müssen.',
    scope: 'world',
    config: true,
    default: true,
    type: Boolean,
    onChange: value => {
      if (value) {
        autoRegisterBabel();
      }

      window.location.reload();
    },
  });

  game.settings.register("pf1e-de", "enableDebug", {
    name: "Activates debug mode",
    hint: "It will display, in addition to a console error, any errors encountered during translation as a notification.",
    scope: "world",
    config: true,
    default: false,
    type: Boolean,
  })

  Babele.get().registerConverters({
    'actions': (value, translations) => converters.actions(value, translations),
    'contextNotes': (originalContextNotes, translatedContextNotes) => converters.contextNotes(originalContextNotes, translatedContextNotes),
    subSchool: converters.translateSubSchool,
    types: converters.translateTypes,
    subSchool: converters.translateSubSchool,
    containerItems: tContainerItems,              // Translate Items in Containers
learnedAt: (learnedAt) => {
  const domainsMap = new Map([
    ["Air", "Luft"],
    ["Animal", "Tiere"],
    ["Artifice", "Handwerk"],
    ["Chaos", "Chaos"],
    ["Charm", "Verzauberung"],
    ["Community", "Gemeinschaft"],
    ["Darkness", "Dunkelheit"],
    ["Death", "Tod"],
    ["Destruction", "Zerstörung"],
    ["Earth", "Erde"],
    ["Evil", "Böses"],
    ["Fire", "Feuer"],
    ["Glory", "Herrlichkeit"],
    ["Good", "Gutes"],
    ["Healing", "Heilung"],
    ["Knowledge", "Wissen"],
    ["Law", "Ordnung"],
    ["Liberation", "Befreiung"],
    ["Luck", "Glück"],
    ["Madness", "Wahnsinn"],
    ["Magic", "Magie"],
    ["Nobility", "Adel"],
    ["Plant", "Pflanzen"],
    ["Protection", "Schutz"],
    ["Repose", "Ruhe"],
    ["Rune", "Runen"],
    ["Scalykind", "Schuppenartige"],
    ["Strength", "Stärke"],
    ["Sun", "Sonne"],
    ["Travel", "Reisen"],
    ["Trickery", "Tricks"],
    ["Void", "Leere"],
    ["War", "Krieg"],
    ["Water", "Wasser"],
    ["Weather", "Wetter"]
  ]);
  domainsMap.forEach((translation, original) => {
    if (learnedAt.domain?.hasOwnProperty(original)) {
      learnedAt.domain[translation] = learnedAt.domain[original];
      delete learnedAt.domain[original];
    }
  });

  const subDomainsMap = new Map([
    ["Aeon", "Aionen"],
    ["Agathion", "Agathionen"],
    ["Alchemy", "Alchemie"],
    ["Ambush", "Hinterhalt"],
    ["Ancestors", "Ahnen"],
    ["Arcane", "Arkane"],
    ["Archon", "Archonten"],
    ["Aristocracy", "Aristokratie"],
    ["Arson", "Brandstiftung"],
    ["Ash", "Asche"],
    ["Azata", "Azata"],
    ["Blood", "Blut"],
    ["Cannibalism", "Kanibalismus"],
    ["Captivation", "Entrückung"],
    ["Catastrophe", "Katastrophen"],
    ["Caves", "Höhlen"],
    ["Chivalry", "Ritterlichkeit"],
    ["Cloud", "Wolken"],
    ["Competition", "Wettstreit"],
    ["Construct", "Konstrukte"],
    ["Cooperation", "Zusammenarbeit"],
    ["Corruption", "Verderbnis"],
    ["Curse", "Flüche"],
    ["Daemon", "Daimon"],
    ["Dark Tapestry", "Dunkles Firmament"],
    ["Day", "Tag"],
    ["Decay", "Verwesung"],
    ["Deception", "Täuschung"],
    ["Defense", "Verteidigung"],
    ["Demodand", "Demodand"],
    ["Demon", "Dämon"],
    ["Devil", "Teufel"],
    ["Divine", "Göttliche"],
    ["Dragon", "Drachen"],
    ["Duels", "Zweikampf"],
    ["Education", "Unterweisung"],
    ["Entropy", "Entropie"],
    ["Espionage", "Spionage"],
    ["Exploration", "Entdeckung"],
    ["Family", "Familie"],
    ["Fate", "Schicksal"],
    ["Fear", "Furcht"],
    ["Feather", "Feder"],
    ["Ferocity", "Wildheit"],
    ["Fist", "Faust"],
    ["Flotsam", "Treibgut"],
    ["Flowing", "Ströme"],
    ["Fortifications", "Befestigungen"],
    ["Freedom", "Freiheit"],
    ["Friendship", "Freundschaft"],
    ["Fur", "Fell"],
    ["Greed", "Gier"],
    ["Growth", "Wachstum"],
    ["Hatred", "Hass"],
    ["Heroism", "Heldentum"],
    ["Home", "Heim"],
    ["Honor", "Ehre"],
    ["Hubris", "Anmassung"],
    ["Ice", "Eis"],
    ["Imagination", "Einbildung"],
    ["Industry", "Geschäftigkeit"],
    ["Inevitable", "Unvermeidbare"],
    ["Innuendo", "Anspielung"],
    ["Insanity", "Irrsinn"],
    ["Insect", "Insekten"],
    ["Isolation", "Isolation"],
    ["Judgment", "Urteil"],
    ["Kyton", "Kytonen"],
    ["Language", "Sprachen"],
    ["Leadership", "Führung"],
    ["Legend", "Legenden"],
    ["Legislation", "Gesetzgebung"],
    ["Leshy", "Lesnik"],
    ["Light", "Licht"],
    ["Lightning", "Blitze"],
    ["Loss", "Verlust"],
    ["Love", "Liebe"],
    ["Loyalty", "Loyalität"],
    ["Lust", "Lust"],
    ["Martyr", "Aufopferung"],
    ["Medicine", "Medizin"],
    ["Memory", "Erinnerung"],
    ["Metal", "Metall"],
    ["Monsoon", "Monsun"],
    ["Moon", "Mond"],
    ["Murder", "Mord"],
    ["Night", "Nacht"],
    ["Nightmare", "Albtraum"],
    ["Oceans", "Meere"],
    ["Petrification", "Versteinerung"],
    ["Plague", "Seuche"],
    ["Portal", "Portale"],
    ["Protean", "Proteaner"],
    ["Psychopomp (Death)", "Seelenbegleiter (Tod)"],
    ["Psychopomp (Repose)", "Seelenbegleiter (Ruhe)"],
    ["Purity", "Reinheit"],
    ["Radiation", "Strahlung"],
    ["Rage", "Wut"],
    ["Redemption", "Erlösung"],
    ["Resolve", "Entschlossenheit"],
    ["Restoration", "Genesung"],
    ["Resurrection", "Wiederbelebung"],
    ["Revelation", "Offenbarung"],
    ["Revelry", "Schwelgerei"],
    ["Revolution", "Revolution"],
    ["Riot", "Aufruhr"],
    ["Rites", "Rituale"],
    ["Rivers", "Flüsse"],
    ["Sahkil", "Sahkil"],
    ["Saurian", "Saurier"],
    ["Seasons", "Jahreszeiten"],
    ["Self-Realization", "Selbstverwirklichung"],
    ["Shadow", "Schatten"],
    ["Slavery", "Sklaverei"],
    ["Smoke", "Rauch"],
    ["Solitude", "Einsamkeit"],
    ["Souls", "Seele"],
    ["Sovereignty", "Herrschaft"],
    ["Stars", "Sterne"],
    ["Storms", "Stürme"],
    ["Tactics", "Taktik"],
    ["Thievery", "Diebstahl"],
    ["Thirst", "Durst"],
    ["Thorns", "Dornen"],
    ["Thought", "Gedanken"],
    ["Toil", "Mühe"],
    ["Torture", "Folter"],
    ["Trade", "Handel"],
    ["Trap", "Fallen"],
    ["Truth", "Wahrheit"],
    ["Tyranny", "Tyrannei"],
    ["Undead", "Untod"],
    ["Venom", "Gift"],
    ["Wards", "Schutzzeichen"],
    ["Whimsy", "Launenhaftigkeit"],
    ["Wind", "Wind"],
  ]);
  subDomainsMap.forEach((translation, original) => {
    if (learnedAt.subDomain?.hasOwnProperty(original)) {
      learnedAt.subDomain[translation] = learnedAt.subDomain[original];
      delete learnedAt.subDomain[original];
    }
  });

  const bloodLinesMap = new Map([
    ["Aberrant", "Abnormale"],
    ["Abyssal", "Dämonische"],
    ["Accursed", "Verfluchte"],
    ["Aquatic", "Aquatische"],
    ["Arcane", "Arkane"],
    ["Astral", "Astrale"],
    ["Boreal", "Arktische"],
    ["Celestial", "Himmlische"],
    ["Daemon", "Dämonische"],
    ["Deep Earth", "Tiefen"],
    ["Destined", "Schicksalhafte"],
    ["Div", "Div"],
    ["Djinni", "Dschinn"],
    ["Draconic", "Drachen"],
    ["Dreamspun", "Traum"],
    ["Ectoplasm", "Ektoplasmatische Blutlinie"],
    ["Efreeti", "Ifrit"],
    ["Elemental", "Elementare"],
    ["Fey", "Feen"],
    ["Ghoul", "Ghoul"],
    ["Harrow", "Turmkarten"],
    ["Imperious", "Gebieterische"],
    ["Impossible", "Unmögliche"],
    ["Infernal", "Teuflische"],
    ["Kobold Sorcerer", "Koboldhexenmeister"],
    ["Maestro", "Maestro"],
    ["Marid", "Mariden"],
    ["Martyred", "Märtyrer"],
    ["Naga", "Naga"],
    ["Nanite", "Naniten"],
    ["Oni", "Oni"],
    ["Orc", "Orkische"],
    ["Pestilence", "Pestilenz"],
    ["Phoenix", "Phönix"],
    ["Possessed", "Heimgesuchte"],
    ["Protean", "Proteanische"],
    ["Psychic", "Mentalmagische"],
    ["Rakshasa", "Rakschasa"],
    ["Salamander", "Salamander"],
    ["Scorpion", "Skorpion"],
    ["Serpentine", "Schlangen"],
    ["Shadow", "Schatten"],
    ["Shaitan", "Schaitan"],
    ["Shapechanger", "Gestaltwandler"],
    ["Solar", "Solare"],
    ["Starsoul", "Stern"],
    ["Stormborn", "Sturm"],
    ["Undead", "Grabes"],
    ["Unicorn", "Einhorn"],
    ["Verdant", "Immergrüne"],
    ["Vestige", "Ahnen"],
  ]);
  bloodLinesMap.forEach((translation, original) => {
    if (learnedAt.bloodline?.hasOwnProperty(original)) {
      learnedAt.bloodline[translation] = learnedAt.bloodline[original];
      delete learnedAt.bloodline[original];
    }
  });

  return learnedAt;
},
  });

  if (game.settings.get('pf1e-de', 'autoRegisterBabel')) {
    autoRegisterBabel();
  }
});

function autoRegisterBabel() {
  if (typeof Babele !== 'undefined') {
    Babele.get().register({
      module: 'pf1e-de',
      lang: 'de',
      dir: 'translations',
    });
  }
}

Hooks.once("ready", async () => {
  if (game.settings.get('core', 'language') !== "de") return;

  // Add new filters
  const browsers = [
    pf1.applications.compendiumBrowser.ClassBrowser,
    pf1.applications.compendiumBrowser.ItemBrowser,
    pf1.applications.compendiumBrowser.SpellBrowser,
    pf1.applications.compendiumBrowser.FeatBrowser,
    pf1.applications.compendiumBrowser.RaceBrowser,
    pf1.applications.compendiumBrowser.CreatureBrowser,
    pf1.applications.compendiumBrowser.BuffBrowser,
  ];
  browsers.forEach((browser) => {
    browser.filterClasses.splice(
      0,
      0,
      OriginalNameFilter,
    );
  });
});
