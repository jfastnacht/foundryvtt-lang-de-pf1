# Changelog
## 11.0
### Changelog
- Hinzufügen neuer Filter für Suche nach Originalname im Kompendiumbrowser (#170)
- Übersetzung neuer Kontextnotes von Krit Talenten (#153)
- Übersetzung neuer Monster-Talente (#144)
- Übersetzung neuer Technologieregeln (#133, #149)
- Übersetzung neuer `Zusätzlicher [...]`-Talente (#131)
- Übersetzung neuer Bewegungstalente (#129)
- Übersetzung neuer Monsterfähigkeiten (#158)
- Übersetzung neuer Okkulter Gegenstände (#154)
- Übersetzung neuer Buffs (#143)
- Übersetzung Erweiterungen an Zaubern (#134, #137, #138)
- Übersetzung neue Tabelle für Gelegenheitsangriffe (#159)
- Übersetzung neue Tabellen für Urmagie und Wilde Magie (#132)

### Migration
- Update Persönlichkeitsspeicherkristalle (#167)
- Update der neuen Kompendiennamen (#152)
- Update `Meistertrick` Schurkentrick und Ninja-Merkmal (#145)
- Entfernen redundanter Statistiken bei Kybernetik (#142)
- Update der Header für Fertigkeitsspezialisierungen (#146)
- Überführung von Verwendungshinweisen in neues Instruktionsfeld (#155)
- Harmonisierung Klassenmerkmale (#135)

### Bugfixes
- Entfernen fehlerhafter `<span>` Elemente (#164)

## 10.7
### Feature
- Hinzufügen viele Compendium Links für Technologie
- Übersetzung niederer Nimmervoller Beutel
- Update Modulname für bessere Auffindbarkeit in Foundry
- Klassenmerkmale mit dem Suffix `(Alternativregeln)` wurden umbennant in `(entfesselt)`
- Hinzufügen fehlender Übersetzungen zu Buffs

### Bugfixes
- Typo in Feuersturm zauber
- Format von Erinnerung Verändern
- Format von Speicherkristallen
- Entfernen unnötiger Leerzeichen in vielen Zaubern
- Fehlende Übersetzung Unheimliches Schicksal
- Entfernen unnötiger Quellen in der Beschreibung
- Fix Typo in Wiederbelebungskammer

## 10.6
### Features
- Ordnerübersetzungen in Spells und Waffen
- Spuk Unterschule
- Übersetzung einiger Waffen

### Bugfixes
- Fixed broken compendium links in technology
- Fix typo in Unauffindbarkeit spell
- Fix format in Compbrower filters

## 10.5
### Features
- Translated new monster rules introduced with v10

### Bugfix
- Fixed typo in technomancer translation

## 10.4
### Features
- Translated Technomancer Prestigeclass
- Translated Aerochemis Archetype
- Translated Blood Alchemist Archetype
- Translated wast amount of missing standard strings (will negates, etc.)
- Translated Rogue (Unchained) base features (tricks excluded)
- Translated Shifter base features (aspects excluded)
- Translated new Sorcerer Features from v10.x
- Translated alchemical reagents (#121)
- Translated new sadels (#120)
- Translated new competence bonus items (#119)
- Translated new combo ability score belts and headbands (#117)
- Translated new Lantern Items (#116)
- Translated bracers of armor (#115)
- Translated updated net items (#114)
- Translated amulett of mighty fists (#113)
- Translated new ration items (#112)
- Translated alchemical power components (#110)
- Translated new mounts (#108)
- Translated food and drinks from v10.0
- Translated all other items that received update in v10 (#111)

### Bugfix
- fixed tooling not ordering entries properly
- fixed Firework spell actions

### Tooling
- added converter for standard action names (throw, attack, use...)

## 10.3
### Features
- Translated several alchemical item (!207)

### Bugfixes
- Fixed a bug where containers would not open when description is not translated (!208)

## 10.2
# Features
- Added automatic translation of container contents like class kits
- Added translation for Hedge Witch and Cryptid Scholar archetype
- Added missing translation of mess kits
- Translated folders in race compendium

# Bugfixes
- Fixed action in Touch of Combustion spell
- Fixed typo in Ray of Enfeeblement
- Fixed some incorrect spell durations that would not be displayed

## 10.1
### Bugfixes
- Added missing folder translation in items compendium
- Added missing tail terror feat translation
- Fixed typo in cybertech ears and eyes
- Fixed error in detect alignment spells

## 10.0
### Features
- Translation of new magic items (!187)
- Move translation of technology content from pf-content to system (!186)
- Translation of new technology feats (!167)
- Translation of top level folders (!185)
- Translation of "big 6" magic items (cloak of resistence, ring of protection, etc.) (!182)
- Translation of various basic class features (!169, !176, !189)
- Translation of new cantrip, orison and knacks class feature (!175)
- Translation of new magical containers (bag of holding, efficient quiver, etc.) (!172)
- Translation of updated adventuring gear (!172, !184)
- Removal of obsolet Dismissable Tag on spells (!171)
- Translation of new spell descriptor rules (!159)
- Translation of new trample, grapple, squeezing and incorporeal rules (!162, !152, !151)
- Translation of new feats introduced with v10 (!158, !163)
- Translation for artistry and lore skills (!155)
- Translation of actions for coin shot spell (!150)
- Adjust Mapping of mythic paths according to v10 (!183)

### Bugfixes
- Fix typo in Align Weapon spell (!157)
- Fix incorrect translation of holy water (!163)
- Fix formats of feats (!154)
- Fix formats of natural attacks rule (!153)
- Various fixes already existing in v9 (!149)

### Tooling
- Added scriot to allow comparison of translation files (!168)
- Added debug mode and error messages for missing context note translation (!181)

## 0.9.0
- Added translation for new feats introduced in pf V9.5
- Added translation for new tech roll tables from pf-content V0.3.6.

#### Bugfix
- Removed several typos in spells
- Removed excessive compendium linking in skill rules compendium

## 0.8.0
### Features
- Added translation for Bloodlines, Domains and Subdomains in spells

#### Bugfix
- Minor formating error with line breaks in compendium browser

## 0.7.3
### Features
- Compatibility Release for PF1e System V9.4

### Bugfix
- fixed error in converter mapping that prevented certain spells from being used

## 0.7.2
### Features

- Added support for V11 compendium folder translation
- Improved technology pack translation
- Compatibility Release for PF1e System V9.3

## 0.7.1
#### Bugfix

- Spells without saving throw info could not be opened (!134)

## 0.7.0
### Features

- Translate pf1 universal monster rules (!131)
- Translated Monster Abilities (!131)
- Translate new item creation feats introduced in V9 (!130)
- Update Compatibility for Foundry V11 and PF System V9

## 0.6.0
### Features

- Add translation for races compendium (!119)
- Add translation for new feats in V9 (!124)
- Add translation for spell school rules compendium introduced in V9 (!117)

### Bug Fixes

- Fix fire shield spell (!128)
- Fix guidance spell (!127)
- Fix compendium link in akashic form spell (!126)

## 0.5.1
### Bug Fixes

- fixed broken edit button on use action caused by missing translation for spells

## 0.5.0
### Features

- Added compendium links for spells, conditions and skills to spell compendium
- Added missing spell translations from several Adventure Paths
- Added missing spell translations from ultimate wilderness
- Added translation for conditions in rules compendium
- fixed translation of schools and subschools.

### Bug Fixes

- removed unnecessary line breaks in feat translation

## 0.4.0
### Features

- Added translation for spells
- Added pf1 system compatibility for Version 0.82.5

## 0.3.0
### Features

- Translated Skills

## 0.2.0
### Features

- Translated Technology compendium
- Translated Feats
- Translated Conditions
- Translated Common Buffs
- Translated Classes
- Translated Racial HD
- Translated mythic paths

## 0.1.0
### Features

- Initial Pull from source project
